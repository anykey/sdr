#!/usr/bin/env python
# -*- coding: utf-8 -*-

# *.arc dump utility.
# 
# This file is part of SDR tools.
# 
# Copyright (C) 2015 Nick Krylov.
# 
# This file is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING3.  If not see
# <http://www.gnu.org/licenses/>.

from argparse import ArgumentParser
from mbs import *
from etypes import TCardinality
import zlib
import os.path as op
import re

(EMMCScBkupOwnerDataTypeDataOwner ,

#    // Relates to java data for a particular owner
    EMMCScBkupOwnerDataTypeJavaData,

#    // Relates to public data for a particular owner
    EMMCScBkupOwnerDataTypePublicData,

#    // Relates to system data for a particular owner
    EMMCScBkupOwnerDataTypeSystemData,

#    // Relates to active data for a particular owner
    EMMCScBkupOwnerDataTypeActiveData,

#    // Relates to passive data for a particular owner
    EMMCScBkupOwnerDataTypePassiveData)=range(6)

EStreamFormatVersion1=1

class BadRecord(IOError):
    def __init__(self):
        super(BadRecord, self).__init__("Bad record header!")

#TODO: make one func
def checkverhdr(mbs):
    inthdr=mbs.ReadUInt32(4)
    if not (inthdr[0]==EStreamFormatVersion1 and inthdr[1]==0 and inthdr[2]==0 and inthdr[3]==0):
        raise BadRecord()

def checkverhdr2(mbs):
    inthdr=mbs.ReadUInt32(3)
    if inthdr[0]!=EStreamFormatVersion1:
        raise BadRecord()

#From sbebufferhandler
# enum TTransferDataType
#         /**
#         TTransferDataType indicates the type of data being transferred from the Secure Backup
#         Engine to the Secure Backup Server or vice versa.
# 
#         @released
#         @publishedPartner
#         */
#         {
#         ERegistrationData = 0, /*!< 0x00000000: deprecated */
#         EPassiveSnapshotData = 1, /*!< 0x00000001: The data is a snapshot for passive backup that includes file details*/
#         EPassiveBaseData = 2, /*!< 0x00000002: The data is passively backup up private data with no previous history */
#         EPassiveIncrementalData = 3, /*!< 0x00000003: The data is passively backed up private data as an increment on a previous base backup */
#         EActiveSnapshotData = 4, /*!< 0x00000004: The data is a snapshot for active backup that includes file details*/
#         EActiveBaseData = 5, /*!< 0x00000005: The data is actively backup up private data with no previous history */
#         EActiveIncrementalData = 6, /*!< 0x00000006: The data is actively backed up private data as an increment on a previous base backup */
#         ECentralRepositoryData = 7, /*!< 0x00000007: deprecated */
#         EPublicFileListing = 8, /*!< 0x00000008: An externalised list of public file entries belonging to a single data owner */
#         };
#         
#     enum TSBDerivedType
#         /**
#         Used to identify the derived type that the base generic type represents
#         
#         @deprecated
#         @publishedPartner
#         */
#         {
#         ESIDDerivedType = 0, /*!< 0x00000000: SID derived type  */
#         EPackageDerivedType = 1, /*!< 0x00000001: Package derived type */
#         ESIDTransferDerivedType = 2, /*!< 0x00000002: SID data transfer derived type  */
#         EPackageTransferDerivedType = 3, /*!< 0x00000003: Package data transfer derived type */
#         EJavaDerivedType = 4, /*!< 0x00000004: Java ID derived type */
#         EJavaTransferDerivedType = 5, /*!< 0x00000005: Java ID derived type */
#         };
#         
#         /**
#         Maximum number of derived types specified in TSBDerivedType
# 
#         @released
#         @publishedPartner
#         */
#     const TUint KMaxDerivedTypes = 6;    
#         
#     enum TPackageDataType
#         /**
#         TPackageDataType indicates the type of package data being
#         transferred from the Secure Backup Engine to the Secure Backup
#         Server or vice versa.
# 
#         @deprecated
#         @publishedPartner
#         */
#         {
#         ESystemData = 0, /*!< 0x00000000: The data is system files (typically executables or resource files) */
#         ESystemSnapshotData = 1, /*!< 0x00000001: The data is a snapshot for system files */
#         };

class SBSecureId:
    def __init__(self,mbs):
        self.secid=mbs.ReadUInt32()

class SBGenericTransferType:
    def __init__(self,mbs):
        self.dnm=mbs.ReadUInt32()

class SBPackageId:
    def __init__(self,mbs):
        self.uid, self.sid, nmlen=mbs.ReadUInt32(3)
        mbs.getRawCopy(nmlen)
        mbs.Seek(nmlen,mbs.SeekOrigin_Current)

class SBSIDTransferType:
    def __init__(self,mbs):
        self.dnm, self.sid, self.tdt=mbs.ReadUInt32(3)

class PackageTransferType:
    def __init__(self,mbs):
        self.dnm, self.uid, self.pdt=mbs.ReadUInt32(3)

def read_buf(mbs):
    alen=mbs.ReadUInt32()
    mbs.Seek(2*alen,mbs.SeekOrigin_Current)
    
class JavaId:
    def __init__(self,mbs):
        read_buf(mbs)
        read_buf(mbs)
        read_buf(mbs)
        read_buf(mbs)

class JavaTransferType:
    def __init__(self,mbs):
        self.dnm, self.jtt=mbs.ReadUInt32(2)
        read_buf(mbs)
        #mbs.Seek(2*hlen,mbs.SeekOrigin_Current)
        

(ESIDDerivedType, #=0
        EPackageDerivedType,#=1
        ESIDTransferDerivedType,
        EPackageTransferDerivedType,
        EJavaDerivedType, #=4
        EJavaTransferDerivedType
        )=range(6)

sbmap={
       ESIDDerivedType:SBSecureId,
       EPackageDerivedType:SBPackageId,
       ESIDTransferDerivedType:SBSIDTransferType,
       EPackageTransferDerivedType:PackageTransferType,
       EJavaDerivedType:JavaId,
       EJavaTransferDerivedType:JavaTransferType,
       #other not neededm but possible
       }

def init_sb(mbs):
    ret=None
    tp=mbs.ReadUInt32()
    if tp in sbmap:
        ret=sbmap[tp](mbs)
    return ret

#NOT compressed! plain data
class TPkgFile:
    def __init__(self,mbs):

        #TFileFixedHeader
        self.iFileNameLength,self.iFileSize,self.iAttributes,_=mbs.ReadUInt32(4)
        self.iModified=mbs.ReadUInt64()

        fnbytelen=2*self.iFileNameLength

        self.name=unicode(mbs.getSz(fnbytelen),"utf16")#.encode("utf16")

        mbs.Seek(fnbytelen,mbs.SCurrent)
        self.data=mbs.getSz(self.iFileSize)
        mbs.Seek(self.iFileSize,mbs.SCurrent)


#Contains: IndexEntry
class DataIndexWId:
    def __init__(self,mbs):
        checkverhdr(mbs)
        self.count=mbs.ReadUInt32()
        self.adv=[]
        for i in range(self.count):
            self.adv.append(IndexEntry(mbs))

    def iter(self):
        for i in self.adv:
            yield i
    def get_list(self):
        ret=[]
        for i in self.adv:
            ret.extend(i.get_list())
        return ret
            
class ActiveData(DataIndexWId):
    pass
class PassiveData(DataIndexWId):
    pass
class SystemData(DataIndexWId):
    pass

#Contains: ArchiveVector
class DataOwners:
    def __init__(self,mbs):
        checkverhdr(mbs)
        self.count=mbs.ReadUInt32()
        self.adv=[]
        for i in range(self.count):
            self.adv.append(ArchiveVector(mbs))
            mbs.ReadUInt32(2)
    def iter(self):
        for i in self.adv:
            yield i
    def get_list(self):
        ret=[]
        for i in self.adv:
            ret.append((i.off,i.sz))
        return ret
#Contains Tuple: FileInfo ArchiveVector
class PublicDataFiles:
    def __init__(self,mbs):
        self.count=mbs.ReadUInt32()
        self.adv=[]
        for i in range(self.count):
            self.adv.append((FileInfo(mbs),ArchiveVector(mbs)))
            #inthdr=mbs.ReadUInt32(2)
    def iter(self):
        for i in self.adv:
            yield i
    
    def get_list(self):
        ret=[]
        for i in self.adv:
            ret.append((i[0].fnm,i[0].sz))
        return ret
            
#Contains: ArchiveDrvAndVector
class IndexEntry: 
    def __init__(self,mbs):
        self.mbs=mbs
        checkverhdr(mbs)
        self.anid=mbs.ReadUInt32()
        count=mbs.ReadUInt32()
        self.adv=[]
        for i in range(count):
            self.adv.append(ArchiveDrvAndVector(mbs))
    def iter(self):
        for i in self.adv:
            yield i
    def get_list(self):
        ret=[]
        for i in self.adv:
            ret.append((i.strdrv(),i.vec.sz))
        return ret
    def get_n(self):
        return len(self.adv)
    def get_data(self,i,system=False):
        ret=""
        dav=self.adv[i]

        self.mbs.Seek(dav.vec.off,self.mbs.SBegin)
        

        datastart=self.mbs.pos()
        
        #NO encryption - so no buffer read

        hdr=self.mbs.ReadUInt32(3) # TBool=int iEncrypted; TInt iBufferSize, iTotalSize; TInt    iCompressedSize, iUncompressedSize;

        if hdr[0]!=0:
            raise IOError("ENCRYPTION not supported!")
        iTotalSize=hdr[2]
        dataend=datastart+iTotalSize

        while self.mbs.pos()<dataend:
            csz,_=self.mbs.ReadUInt32(2)
            ret+=zlib.decompress(self.mbs.getSz(csz))
            self.mbs.Seek(csz,self.mbs.SCurrent)
        return ret
        
class Index: 
    def __init__(self,mbs):
        self.iType=mbs.ReadUInt8()
        self.av=ArchiveVector(mbs)
        mbs.ReadUInt32(2)
        
class ArchiveDrvAndVector: 
    def __init__(self,mbs):
        #inthdr=mbs.ReadUInt32(4)
        checkverhdr(mbs)
        self.drv=mbs.ReadUInt8()
        self.vec=ArchiveVector(mbs)
    def strdrv(self):
        return chr(ord('A')+self.drv)
        
class ArchiveVector: 
    def __init__(self,mbs):
        self.mbs=mbs
        inthdr=mbs.ReadUInt32(5)
        if inthdr[0]!=EStreamFormatVersion1:
            raise BadRecord()
        self.off=inthdr[1]
        self.sz=inthdr[2]

#     def get_data(self):
#         self.mbs.Seek(self.off)
#         return self.mbs.getRawCopy(self.sz)

#NOTE: expandunicode needed
class FileInfo: 
    def __init__(self,mbs):

        checkverhdr(mbs)
        #HBUFC - len + unicode
        #Tcardinal with len*2, and THEN unicode compressed
        tclen=TCardinality(mbs)
        #Then compressed unicode. BUT 4 now just ascii - so half len
        #something like punycode - http://tools.ietf.org/html/rfc3492.html
        #Real info here http://www.unicode.org/reports/tr6/

        buflen=tclen.val>>1
        self.fnm=mbs.getSz(buflen)
        mbs.Seek(buflen,mbs.SCurrent)
        
        self.dt=mbs.ReadUInt64()
        self.sid,self.sz,self.attr=mbs.ReadUInt32(3)

class DataOwnerCollection: 
    def __init__(self,mbs):
        checkverhdr(mbs)
        self.szr=DriveSizer(mbs)

class DriveAndSize: 
    def __init__(self,mbs):
        checkverhdr2(mbs)
        self.drv=mbs.ReadUInt8()
        self.sz=mbs.ReadUInt32()
        
class DriveSizer: 
    def __init__(self,mbs):
        #Another hdr variant
        
        checkverhdr2(mbs)
        self.dtype=mbs.ReadUInt8()
        self.numee=mbs.ReadUInt8()
        self.ds=[]
        for i in range(self.numee):
            self.ds.append(DriveAndSize(mbs))

tpmap={
EMMCScBkupOwnerDataTypeDataOwner:DataOwners ,

#    // Relates to java data for a particular owner
    EMMCScBkupOwnerDataTypeJavaData:None,

#    // Relates to public data for a particular owner
    EMMCScBkupOwnerDataTypePublicData:PublicDataFiles,

#    // Relates to system data for a particular owner
    EMMCScBkupOwnerDataTypeSystemData:SystemData,

#    // Relates to active data for a particular owner
    EMMCScBkupOwnerDataTypeActiveData:ActiveData,

#    // Relates to passive data for a particular owner
    EMMCScBkupOwnerDataTypePassiveData:PassiveData
    }

class Footer:
    def __init__(self,mbs):
        checkverhdr(mbs)
        inthdr=mbs.ReadUInt32(2)
        count=inthdr[-1]
        self.avlist=[]
        for i in range(count):
            self.avlist.append(Index(mbs))
        self.doc=DataOwnerCollection(mbs)
        tpi=self.tpidx={}

        for idx in self.avlist:
            if tpmap[idx.iType] is not None:
                mbs.Seek(idx.av.off,mbs.SBegin)
                tpi[idx.iType]=tpmap[idx.iType](mbs)
    
    def fiter(self):
        def get_tp_name(itp):
            return tpmap[itp].__name__
        for k,v in self.tpidx.items():
            nm=get_tp_name(k)
            issys=nm=="SystemData"
            if nm=="PassiveData" or issys:
            #Just files in TPkgFile format
                for ie in v.iter():
                    for n in range(ie.get_n()):
                        data=ie.get_data(n,issys)
                        mbsloc=MBS(data)
                        #Size of header
                        if issys:
                            sz=mbsloc.ReadUInt32()
                            mbsloc.Seek(sz,mbsloc.SCurrent)
                        while mbsloc.avail()>0:
                            tpf=TPkgFile(mbsloc)
                            yield tpf

#TODO: mod time also!
def dump_file(pref,symbnm,data):
    fnm=symbnm.replace("\\","/")
    relpath=fnm[3:] # Drop drive
    fname=op.basename(relpath)
    reldir=op.dirname(op.join(pref,relpath))
    if not os.access(reldir, os.F_OK):
        os.makedirs(reldir)
    with open(op.join(reldir,fname),"w") as f:
        f.write(data)

def read(fnm):
    mbs=MBS(MBS.map_buf(fnm))
    inthdr=mbs.ReadUInt32(4+1+1+1+1+1+1+5)
    FooterLengt=inthdr[5]
    slen=mbs.ReadUInt8()
    phonemdl=mbs.getStr(slen)
    if inthdr[2]!=0xbaccccc:
        raise IOError("Unknown archive format!")
    print "Hdr uid3",hex(inthdr[2]),"FooterLengt",FooterLengt,"phonemdl",phonemdl
    mbs.Seek(-FooterLengt,mbs.SEnd)
    print mbs.size(), mbs.off,FooterLengt,FooterLengt+mbs.off 
    return Footer(mbs)

def main():
    parser = ArgumentParser()

    parser.add_argument("-f", "--fbackup",type=str, help="Backup file.")
    parser.add_argument("-d", "--dest",type=str, help="Backup export dir.")
    parser.add_argument( "--filter",type=str,  default=None, help="Filenames filter regexp.")
    args = parser.parse_args()
    
    if args.fbackup is not None:
        fnm=args.fbackup
        footer=read(fnm)
        reflt=None
        if args.filter:
            reflt=re.compile(args.filter)
        for tpkg in footer.fiter():
            fnm=tpkg.name
            if reflt and reflt.search(fnm) is None:
                continue
            print fnm
            if args.dest is not None:
                dump_file(args.dest,fnm,tpkg.data)
                
if __name__ == '__main__':
    main()
