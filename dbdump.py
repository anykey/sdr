#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Symbian EDBMS dump utility.
# 
# This file is part of SDR tools.
# 
# Copyright (C) 2015 Nick Krylov.
# 
# This file is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING3.  If not see
# <http://www.gnu.org/licenses/>.

from edbms import DB
import xml.etree.cElementTree as ET
import csv
from argparse import ArgumentParser

def main():
    parser = ArgumentParser()
    parser.add_argument("-f", "--db",type=str, help="DB file.")
    parser.add_argument("-d", "--dest",type=str, help="Output xml file.")
    parser.add_argument("-c", "--csv",default=False,action="store_const", const=True, help="Output tables to csv files. Encoding is utf8.")
    args = parser.parse_args()

    if args.db is not None and args.dest is not None:
        
        db=DB(args.db)
        
        root=db.alltables2etree()
        if args.csv:
            for tbl in root:
                with open(args.dest+"_"+tbl.attrib["name"]+".csv","wb") as f:
                    csvwrtr = csv.writer(f)
                    #Column names as header
                    schm=tbl[0]
                    colnames=[c.attrib["name"] for c in schm]
                    csvwrtr.writerow(colnames)
                    ncols=len(colnames)
                    #Now data rows
                    rows=tbl[1]
                    for r in rows:
                        csvrow=[None]*ncols
                        for v in r:
                            ic=int(v.attrib["column"])
                            csvrow[ic]=v.text.encode("utf-8")
                        csvwrtr.writerow(csvrow)
        else:
            with open(args.dest,"w") as f:
                f.write(ET.tostring(root,"utf8"))
        
if __name__ == '__main__':
    main()
