# -*- coding: utf-8 -*-

# Symbian EDBMS related classes.
# 
# This file is part of SDR tools.
# 
# Copyright (C) 2015 Nick Krylov.
# 
# This file is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING3.  If not see
# <http://www.gnu.org/licenses/>.

from mbs import *
from etypes import HBuf,TCardinality
from epfs import PermanentFileStore
import xml.etree.cElementTree as ET
import base64
import datetime

KMaxClustering=KMinInlineLimit=16

class ColumnDef:
    EDbColBit,EDbColInt8,EDbColUint8,EDbColInt16,EDbColUint16,EDbColInt32,\
    EDbColUint32,EDbColInt64,EDbColReal32,EDbColReal64,EDbColDateTime,\
    EDbColText8,EDbColText16,EDbColBinary,EDbColLongText8, EDbColLongText16,\
    EDbColLongBinary=xrange(17)
    typename=["EDbColBit","EDbColInt8","EDbColUint8","EDbColInt16","EDbColUint16","EDbColInt32",\
    "EDbColUint32","EDbColInt64","EDbColReal32","EDbColReal64","EDbColDateTime",\
    "EDbColText8","EDbColText16","EDbColBinary","EDbColLongText8", "EDbColLongText16",\
    "EDbColLongBinary"]
    rd=[None,i8(),ui8(),i16(),ui16(),i32(),ui32()]
    ENotNull=0x01
    EAutoIncrement=0x02
    
    def __init__(self,mbs):
        self.name=HBuf(mbs).buf
        v=ui8()
        mbs>>v
        tp=self.tp=v.v()
        mbs>>v
        self.attr=v.v();self.colmxlen=-1
        
        if tp>=self.EDbColText8 and tp<=self.EDbColBinary:
            mbs>>v
            self.colmxlen=v.v()
        
    def isNotNull(self):
        return self.attr&self.ENotNull
    
    def isAutoIncrement(self):
        return self.attr&self.EAutoIncrement
    
    def get_max_len(self):
        return self.colmxlen
    
    def get_type_name(self):
        return self.typename[self.tp]
    
class StoreDef:
    
    def __init__(self,mbs):
        #CDbStoreDef* CDbStoreDef::NewL
        #name - max 64 symb
        self.tblname=HBuf(mbs).buf
        ncol=TCardinality(mbs).val
        
        self.columns=[]
        for i in xrange(ncol):
            self.columns.append(ColumnDef(mbs))
        
        self.cluster_count=TCardinality(mbs).val
        vtok=v=ui32()
        mbs>>v
        self.iTokenId=v.v()
        indexes_count=TCardinality(mbs).val
        
        #Skip indices
        v=ui8()
        v2=ui8()
        for i in xrange(indexes_count):
            nm=HBuf(mbs).buf
            mbs>>v # Comparison
            mbs>>v2 # IsUnique
            cntmax=TCardinality(mbs).val
            
            for j in xrange(cntmax):
                nmkc=HBuf(mbs).buf #aKeyCol.iName - des
                mbs>>v # iLength
                mbs>>v2 # iOrder
            
            mbs>>vtok
    
class TToken:
    
    def __init__(self,mbs):
        v=ui32()
        mbs>>v
        self.head=v.v()
        mbs>>v
        self.next=v.v()
        self.count=TCardinality(mbs).val
        mbs>>v
        self.ia=v.v()

class DB:
    unixepoch=datetime.datetime(1970,1,1,0,0,0)
    secondsperday=24*60*60
    epochshift=62168256000# shift between 0AD epoc32 start date and unix 1970-based start date in seconds
    def __init__(self,fnm):
        self.clmap={}
        
        self.mbs=MBS(MBS.map_buf(fnm))
        self.pfs=PermanentFileStore(self.mbs)
        r=self.pfs.Root()
        schm=self.pfs.getStreamData(r)
        mbs=MBS(schm)
        uid=ui32()
        mbs>>uid
        
        KDbmsStoreDatabase=268435561
        EDbStoreVersion2=3
        if uid.v()!=KDbmsStoreDatabase:
            raise Exception("Bad DbmsStore UID!")
        
        ver=ui8()
        mbs>>ver
        if ver.v()!=EDbStoreVersion2:
            raise Exception("Bad db store version!")
        #Now load schema
        iTokenId=ui32()
        mbs>>iTokenId
        ntbls=TCardinality(mbs).val
        self.tbls=[]
        for i in xrange(ntbls):
            self.tbls.append(StoreDef(mbs))
        
        ptokdata=self.pfs.getStreamData(iTokenId.v())
        mbsptok=MBS(ptokdata)
        iFlags=ui8()
        iPoolToken=Stu([ui32,ui32])
        mbsptok>>iFlags>>iPoolToken
        self.iFlags=iFlags.v()
        self.iPoolToken=iPoolToken.astuple()

    def read_clust(self,idx,with_memb=False):
        try:
            return self.clmap[idx]
        except:
            pass
        v=ui32()
        vv=ui16()
        fclustdata=self.pfs.getStreamData(idx)
        mbsclst=MBS(fclustdata)
        mbsclst>>v
        class Clust:
            pass
        cldes=Clust()
        cldes.iNext=v.v()
        mbsclst>>vv
        membership=cldes.iMemb=vv.v()
        if with_memb:
            members=membership
            allmemb=[]
            val=0
            while members!=0:
                if (members&1):
                    sz=TCardinality(mbsclst).val
                    val+=sz
                allmemb.append(val)
                members>>=1
            
            add=[allmemb[-1]]*(16-len(allmemb))
            cldes.membmap=[0]+allmemb+add #??
            #Read the cluster
            totsz=val
            if totsz>0:
                #REST is data for records
                cldes.recs=mbsclst.getSz(totsz)
                
            self.clmap[idx]=cldes # Cache only full read clusters
        return cldes
    
    class Iter:
        
        def __init__(self,t,cl,idx):
            self.tbl=t
            self.cl = cl
            self.idx = idx
            self.des = None
            
    BEG,NEXT=xrange(2)
    def Goto(self,pos):
        i=self.iter
        index=i.idx # current - begin =0
        if pos==self.BEG:
            i.cl=self.tt.head
            i.des=self.read_clust(i.cl)
            index=-1

        #Always do NEXT, as we do not need other options here!
        membr=i.des.iMemb
        
        while index<=KMaxClustering:
            index+=1
            if (membr>>index)&1:
                i.idx=index
                return True
            
        i.cl=i.des.iNext
        
        if i.cl==0:
            return False
        i.des=self.read_clust(i.cl)
        i.idx=-1
        return self.Goto(self.NEXT)

    def get_all_records_cl(self):
        
        class TReadBitSequence:
            
            def __init__(self):
                self.iBits=0

            def HasBits(self):
                return self.iBits&0x2000000
            
            def Read(self,mbs):
                b=ui8()
                self.iBits>>=1
                if (self.iBits&0x1000000)==0:
                    mbs>>b
                    self.iBits=b.v()| 0xff000000
                return self.iBits&1
        
        #get all from cur clust
        cldes=self.read_clust(self.iter.cl,True)
        index=self.iter.idx
        
        if not ((cldes.iMemb>>index)&1):
            raise Exception("Index {} not found!".format(index))
        off=cldes.membmap[index]
        sz=cldes.membmap[index+1]-off
        arec=cldes.recs[off:off+sz]
        
        mbs=MBS(arec)
        
        szrec=TCardinality(mbs).val
        szrec<<=2
        if(szrec<0):
            raise Exception("szrec<0 !")
        
        This=TReadBitSequence()
        
        row=[]
        b=ui8()
        i4=ui32()
        f4=f()
        dbl=d8()
        
        for ic,c in enumerate(self.iter.tbl.columns):
            if c.attr&c.ENotNull or This.Read(mbs):
                tp=c.tp
                value=None
                if tp==c.EDbColBit:
                    value=bool(This.Read(mbs))
                elif tp<len(c.rd):
                    rd=c.rd[tp]
                    mbs>>rd
                    value=rd.v()
                elif tp==c.EDbColReal32:
                    value=0
                    mbs>>f4
                    value=f4.v()
                elif tp==c.EDbColReal64:
                    value=0
                    mbs>>dbl
                    value=dbl.v()
                elif tp==c.EDbColInt64 or tp==c.EDbColDateTime:
                    value=0
                    mbs>>i4
                    value|=i4.v()
                    mbs>>i4
                    value|=i4.v()<<32
                    if tp==c.EDbColDateTime:
                        if value<0:
                            raise Exception("BC Dates not implemented!")
                        
                        micro=1000000
                        ms=value%micro
                        s=value/micro
                        s-=self.epochshift
                        
                        d=s/self.secondsperday
                        s=s%self.secondsperday
                        value=self.unixepoch+datetime.timedelta(days=d,seconds=s,milliseconds=ms)

                elif tp==c.EDbColText16:
                    value=mbs.ExpandUnicode(TCardinality(mbs).val)
                elif tp==c.EDbColText8 or tp==c.EDbColBinary:
                    mbs>>b
                    value=mbs.getStr(b.v())
                elif tp==c.EDbColLongText8 or tp==c.EDbColLongText16 or tp==c.EDbColLongBinary:
                    if This.Read(mbs)==0:
                        anid0=TCardinality(mbs).val
                        anid=(anid0>>4)|(((anid0<<28)&0xffffffff)>>4)
                        sz=TCardinality(mbs).val
                        recdata=self.pfs.getStreamData(anid)
                        value=recdata
                        if tp==c.EDbColLongText16:
                            mbsloc=MBS(recdata)
                            value=mbsloc.ExpandUnicode(len(recdata))
                    elif tp!=c.EDbColLongText16:
                        #LongText8
                        mbs>>b
                        value=mbs.getStr(b.v())
                    else:
                        #LongText16
                        value=mbs.ExpandUnicode(TCardinality(mbs).val)
                else:
                    raise Exception("Unsupported!"+str(tp))
                row.append((ic,value))
        return row

    def get_all_records(self):
        ret=[]
        while True:
            ret.append(self.get_all_records_cl())
            if not self.Goto(self.NEXT):
                break
        return ret
    
    def rewind2table(self,tbl):
        recdata=self.pfs.getStreamData(tbl.iTokenId)
        mbs=MBS(recdata)
        self.tt=TToken(mbs)
        self.iter=self.Iter(tbl,self.tt.head,0)
        
    def table2etree(self,tbl,root):
        self.rewind2table(tbl)
        #Schema
        ettbl=ET.SubElement(root, "Table")
        ettbl.attrib["name"]=tbl.tblname
        etschm=ET.SubElement(ettbl, "Schema")
        for c in tbl.columns:
            col=ET.SubElement(etschm, "Column")
            col.attrib["name"]=c.name
            col.attrib["type"]=c.get_type_name()
            col.attrib["maxlen"]=str(c.get_max_len())
            if(c.isNotNull()):
                col.attrib["notnull"]="1"
            if(c.isAutoIncrement()):
                col.attrib["autoincr"]="1"

        #Rows
        etrows=ET.SubElement(ettbl, "Rows")
        if self.Goto(self.BEG):
            for r in self.get_all_records():
                etrow=ET.SubElement(etrows, "Row")

                for ic,val in r:
                    etval=ET.SubElement(etrow, "Value")
                    etval.attrib["column"]=str(ic)
                    c=tbl.columns[ic]
                    
                    if c.tp==c.EDbColDateTime:
                        etval.text=val.isoformat(" ")#NOTE: Symbian date and time are local!
                    elif c.tp==c.EDbColBinary or c.tp==c.EDbColLongBinary:
                        etval.text=base64.b64encode(val)
                    elif isinstance(val,unicode):
                        etval.text=val.replace(unichr(0x2029),"\n")#Convert unicode newline to normal, making text more readable
                    else:
                        etval.text=str(val)

    def alltables2etree(self):
        root=ET.Element("Tables")
        for t in self.tbls:
            self.table2etree(t,root)
        return root
