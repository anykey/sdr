# -*- coding: utf-8 -*-

# Helpers for reading binary files and streams.
# 
# This file is part of SDR tools.
# 
# Copyright (C) 2015 Nick Krylov.
# 
# This file is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING3.  If not see
# <http://www.gnu.org/licenses/>.

import os
import mmap
import struct

class TPBase(object):
    tcode=""
    end="<" # Little-endian for epoc. ALWAYS must be set! Otherwise native sizes may break things up!
    def __init__(self,N=1):
        self.rcode=struct.Struct(self.end+str(N)+self.tcode) 
        self.sz=self.rcode.size
    
    def read(self,mbs):
        self.val=mbs.unpack(self.rcode,self.sz)[0]
    
    def __str__(self):
        return str(self.val)

    #TODO: Some simple one symbol getter here like ! or * ?
    def v(self):
        return self.val

class Stu(TPBase):
    
    def __init__(self,clss):
        self.classes=[f() for f in clss]
    
    def read(self,mbs):
        for cl in self.classes:
            cl.read(mbs)
    
    def astuple(self):
        return tuple((cl.v()  for cl in self.classes))
    
    def __str__(self):
        t=self.astuple()
        return str(t)

tpmap={"i8":"b",
       "i16":"h",
       "i32":"i",
       "i64":"q",
       
       "ui8":"B",
       "ui16":"H",
       "ui32":"I",
       "ui64":"Q",
       
       "f":"f",
       "d8":"d",
       }

glb=globals()

for k,v in tpmap.items():
    tp=type(k,(TPBase,),dict(TPBase.__dict__))
    tp.tcode=v
    glb[k]=tp

class MBS:
    SBegin,SCurrent,SEnd=xrange(3)
    
    @classmethod
    def map_buf(cls,fnm):
        f=os.open(fnm ,os.O_RDONLY)
        return mmap.mmap(f,0,access=mmap.ACCESS_READ)
    
    def __init__(self,b): # from string/buf or file
        self.buf=b
        self.off=0
    
    def __rshift__(self,o):
        o.read(self)
        return self

    def Seek(self,pos,d):
        if d==self.SBegin:
            self.off=pos
        elif d==self.SCurrent:
            self.off+=pos
        elif d==self.SEnd:
            self.off=self.size()+pos
        else:
            raise IOError("Bad direction type!")

    def unpack(self,c,sz):
        ret=c.unpack_from(self.buf,self.off)
        self.off+=sz
        return ret
    
    def pos(self):
        return self.off
    
    def size(self):
        return len(self.buf)
    
    def avail(self):
        return self.size()-self.off
    
    def get(self,o,e):
        return self.buf[o:e]
    
    def getStr(self,sz):
        ret=self.getSz(sz)
        self.off+=sz
        return ret
    
    def getSz(self,sz,start=None):
        pos=self.off if start is None else start
        return self.buf[pos:pos+sz]
    #Compat helpers:
    def ReadUInt8(self):
        v=ui8();v.read(self)
        return v.v()
    
    def ReadUInt32(self,N=1):
        l=[]
        for i in xrange(N):
            l.append(ui32)
        
        v=Stu(l)
        v.read(self)
        if N>1:
            return v.astuple()
        else:
            return v.astuple()[0]
    
    def ReadUInt64(self):
        v=ui64();v.read(self)
        return v.v()
    
    #http://www.unicode.org/reports/tr6/
    SW=[0x0000,
        0x0080,#     Latin-1 Supplement 
        0x0100,#     Latin Extended-A
        0x0300,#     Combining Diacritical Marks
        0x2000,#     General Punctuation 
        0x2080,#    Currency Symbols
        0x2100,#    Letterlike Symbols and Number Forms
        0x3000    #CJK Symbols & Punctuation
    ]
    OffsetTable=[None]+[0x80*x for x in xrange(1,68)]+\
    [0x80*x+0xac00 for x in xrange(68,0xA8)]+\
    [None for x in xrange(0xA8,0xA9)]+\
    [0x00C0,0x0250,0x0370,0x0530,0x3040,0x30A0,0xFF60]
    
    def ExpandUnicode(self,nbytes):
        DW=[0x0080,# /* Latin-1 */
            0x00C0, #/* Latin Extended A */
            0x0400, #/* Cyrillic */
            0x0600, #/* Arabic */
            0x0900, #/* Devanagari */
            0x3040, #/* Hiragana */
            0x30A0, #/* Katakana */
            0xFF00, #/* Fullwidth ASCII */
            ]
        SQ0=0x01
        SQ7=0x08 
        SDX=0x0B
        SQU=0x0E
        SCU=0x0F
        SC0=0x10
        SC7=0x17
        SD0=0x18
        SD7=0x1F
        #Unicode mode
        UC0=0xE0
        UC7=0xE7
        UD0=0xE8
        UD7=0xEF
        UQU=0xF0
        UDX=0xF1
        
        dynwcur=0
        isuni=False
        
        def pass_throught(b):
            return b == 0x0000 or b == 0x0009 or b == 0x000A or b == 0x000D or \
(b >= 0x0020 and b <= 0x007F)
        def stw(b,w):
            return unichr(self.SW[w]+b) 
        
        def dynw(b,w):
            return unichr(DW[w]+b-0x80)
        
        def read_qchr(b,w):
            b=(b)
            if b>=0x80:
                return dynw(b,w)
            return stw(b,0)
            
        instr=self.getStr(nbytes)
        
        def rd_uchar(istr):
            hb,lb=istr[:2]
            return unichr(ord(hb)<<8|ord(lb))
        
        def switch_dw_ext(hb,lb):
            hb,lb=ord(hb),ord(lb)
            n=hb>>5
            DW[n]= 0x10000 + (0x80 * ((hb & 0x1F) * 0x100 + lb))
            return n
            
        def switch_dw(n,idx):
            DW[n]=self.OffsetTable[ord(idx)]
            return n
        
        outstr=u''
        i=0
        while i<nbytes:
            b=instr[i]
            b=ord(b)
            uch=None
            if isuni:
                #TBool TUnicodeExpander::HandleUByteL(TUint8 aByte)
                if b==UQU :
                    i+=1
                    uch=rd_uchar(instr[i:i+2])
                    i+=1
                elif b==UDX :
                    i+=1
                    switch_dw_ext(instr[i:i+2])
                    i+=1
                    isuni=False
                elif UC0<=b and b<=UC7:
                    isuni=False
                    dynwcur=b-SC0
                elif UD0<=b and b<=UD7:
                    n=b-UD0
                    i+=1
                    dynwcur=switch_dw(n,instr[i])
                    isuni=False
                elif 0xF3<=b and b<=0xDF:
                    uch=rd_uchar(instr[i:i+2])
                    i+=1
                else:
                    raise Exception("Decode ERROR!")
            else:
                #TBool TUnicodeExpander::HandleSByteL(TUint8 aByte)
                if pass_throught(b):
                    uch=unichr(b)
                elif b>=0x80:
                    uch=dynw(b,dynwcur)
                elif b==SQU:
                    i+=1
                    uch=rd_uchar(instr[i:i+2])
                    i+=1
                elif b==SCU:
                    isuni=True
                elif b==SDX:
                    i+=1
                    switch_dw_ext(instr[i:i+2])
                    i+=1
                elif SQ0<=b and b<=SQ7:
                    #NO lock - just one char
                    n=b-SQ0
                    i+=1
                    uch=read_qchr(instr[i],n)
                elif SC0<=b and b<=SC7:
                    dynwcur=b-SC0
                elif SD0<=b and b<=SD7:
                    n=b-SD0
                    i+=1
                    dynwcur=switch_dw(n,instr[i])
                else:
                    raise Exception("Decode ERROR!")
            
            if uch is not None:
                outstr+=uch
            i+=1
        
        return outstr
