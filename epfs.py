# -*- coding: utf-8 -*-

# PermanentFileStore reading implementation.
# 
# This file is part of SDR tools.
# 
# Copyright (C) 2015 Nick Krylov.
# 
# This file is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING3.  If not see
# <http://www.gnu.org/licenses/>.

from mbs import *

# CPermanentStoreToc class
# 
# Represents the data kept in the permanent file store TOC (Table Of Content).
# Each TOC consists of:
#  - TOC header         - CPermanentStoreToc::STocHead structure;
#  - set of TOC entries - CPermanentStoreToc::TEntry structure;
# 
# Each TOC entry consists of:
#  - A stream handle (32 bits: MSB - invalid/deleted, 3 bits - unused, 4 bits - generation counter, 24 bits - stream handle);
#  - A stream ref - the offset of the stream data in the permannet file store;
# 
# STocEntry
# {
#     TInt32 handle;
#     TInt32 ref;
# };
# TEntry
# {
#     TInt handle;
#     TInt ref;
# }
# STocHead
# {
#     TInt32 primary;
#     TInt32 avail;
#     TUint32 count;
# };

class PermanentFileStore:
    KMaskHandleIndex=0xffffff
    
    def __init__(self,mbs):
        self.mbs=mbs
        chk=Stu((ui32,ui32,ui32,ui32))
        mbs>>chk
        
        # TODO: check this is good uids
        chkt=chk.astuple()
        if chkt[0]!=268435536 or chkt[1]!=268439230:#CPermanentFileStore uids
            raise IOError("Bad PFS header!")
        
        KHandleTocBase=0x40000000
        
        psh=Stu((ui32,ui32,ui32,ui16))
        mbs>>psh 
        psht=psh.astuple()
        #iBackup, iHandle, iRef, iCrc=psht
        iBackup, iHandle, iRef, _=psht
        toc=iBackup>>1
        if iBackup&1:
            self.iReloc=0
            iTarget=0
        else:
            handle=iHandle
            ref=iRef
            
            if handle==0 and toc!=ref:
                toc=ref
                iBackup=toc<<1
            
            #NOW some wird check, not needed 4 now
            self.iReloc=handle
            iTarget=ref
        iToc=toc
        
        KFrameNonexistent16=-1
        KOffsetTocHeader=-12

        #Then we check cache, delta tables, then toc
        #From CPermanentStoreToc::NewL
        aBaseReloc= iTarget if self.iReloc==KHandleTocBase else KFrameNonexistent16
        aToc=iToc
        if 0==aToc:
            return
        aToc+=KOffsetTocHeader #relative to Base(+32)
        
        self.KSizeFrameDes16=2
        self.KShiftFrameDes16=1
        self.KShiftFrameLength16=14
        
        self.aBase=32 #Fixed 4 now
        self.KMaskFrameLength16=0x3fff

        self.KFrameFullLength16=self.KMaskFrameLength16+1
        
        self.OpenStrm(aToc)

        #Now read STocHead
        stoch=Stu((ui32,ui32,ui32))
        mbs>>stoch
        primary,_,count=stoch.astuple()

        KHandleInvalid=0x80000000
        KHandleInvalidMask=0x7fffffff
        KTocDeltaCap=64
        KSizeTocDeltaExtra=7
        KSizeTocDeltaEntry=8
        KMaxTUint16=0xffff
        KMaxTocDeltaMagic= KMaxTUint16
        
        KTocDelta=KHandleInvalid

        KSizeTocEntry=5
        self.primary=primary&KHandleInvalidMask # cant do like in C
        iCount=count
        iOff = aToc
        entries=[]
        if primary & KTocDelta:
            #In TOC delta: InternalizeL
            def load_toc(strm):
                tco=i32()
                strm>>tco
                tocoff = tco.v()
                if KFrameNonexistent16!=aBaseReloc:
                    tocoff = aBaseReloc - KOffsetTocHeader
                if tocoff<0 or tocoff>=iOff:
                    raise IOError("Bad TOC offset!")
                imgc=ui16()
                strm>>imgc
                iMagic = imgc.v()
                def IsDelta():
                    delta=len(entries)
                    if delta >= KTocDeltaCap:
                        return False
                    magic = iMagic + delta
                    if magic > KMaxTocDeltaMagic:
                        return False
                    return magic*KSizeTocDeltaEntry <= iCount*KSizeTocEntry - KSizeTocDeltaExtra
                if(not IsDelta()):
                    raise Exception("Storage corrupted!")
                
                nn=ui8()
                strm>>nn
                n=nn.v()
                
                last = 0
                stoce=Stu((i32,i32))
                for i in xrange(n):
                    strm>>stoce
                    entr=handle,ref=stoce.astuple()
                    i=handle&self.KMaskHandleIndex
                    if i<=last or i > iCount:
                        raise IOError("!")
                    last = i
                    entries.append(entr)
                return tocoff
                
            aToc =load_toc(mbs)+KOffsetTocHeader
            self.OpenStrm(aToc)
            mbs>>stoch
            self.primary,_,count=stoch.astuple()
            
        maintoc=[]
        stocme=Stu((ui8,i32))
        for i in xrange(count):
            mbs>>stocme
            entr=handle,ref=stocme.astuple()
            maintoc.append(entr)
        self.maintoc=maintoc
        
        size=count*KSizeTocEntry
        
        KOffsetTocHeader=-12
        
        size-=KOffsetTocHeader
        
        self.delta=entries

    def Position(self,anOffset):
        return self.aBase+(anOffset+(anOffset>>self.KShiftFrameLength16<<self.KShiftFrameDes16))

    def OpenStrm(self,anOffset):
        
        pos=self.Position(anOffset)-self.KSizeFrameDes16 # aBase here is const
        frame=ui16()
        self.mbs.Seek(pos,self.mbs.SBegin)
        self.mbs>>frame
        frame=frame.v()
        #TODO: checks read is ok  - !(frame^aMode)&KMaskFrameType16
        
        _in=anOffset&self.KMaskFrameLength16
        frame&=self.KMaskFrameLength16
        KFrameOpen16=0
        if frame==KFrameOpen16:
            frame=self.KFrameFullLength16-_in
        
        if _in+frame>self.KFrameFullLength16:
            raise IOError("!")
        
        anExtent=anOffset+frame
        
        lim=(anOffset&~self.KMaskFrameLength16)+self.KFrameFullLength16
        if anExtent>0 and lim>anExtent:
            lim=anExtent
        return (self.Position(anOffset),self.Position(lim))
    
    def Root(self):
        return self.primary
    
    def Search(self,aHandle):
        #Just plain search
        off=None
        for e,ref in self.delta:
            if e==aHandle:
                off=ref
                break
        if off is None:
            idx=aHandle&self.KMaskHandleIndex
            _,off= self.maintoc[idx-1]
            if self.iReloc==aHandle:
                trg=self.iTarget
                if trg==off:
                    self.iReloc=0
                else:
                    off=trg
        ext=min(0,off)
        return off,ext
    
    def get(self,off):
        off,ext=self.OpenStrm(off)
        part=self.mbs.get(off,ext)
        return part
    
    def getStreamData(self,h):
        off,_=self.Search(h)
        return self.get(off)
