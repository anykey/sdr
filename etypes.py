# -*- coding: utf-8 -*-
# Symbian types support classes.
# 
# This file is part of SDR tools.
# 
# Copyright (C) 2015 Nick Krylov.
# 
# This file is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING3.  If not see
# <http://www.gnu.org/licenses/>.

from mbs import *
class TCardinality:
    KShiftCardinality8=1
    KShiftCardinality16=2
    KShiftCardinality32=3
    
    def __init__(self,mbs):
        v=ui8()
        mbs>>v;n=v.v()
        if ((n&0x1)==0):
            n>>=self.KShiftCardinality8
        elif ((n&0x2)==0):
            mbs>>v
            n+=v.v()<<8;
            n>>=self.KShiftCardinality16;
        elif ((n&0x4)==0):
            mbs>>v;b0=v.v()
            mbs>>v;b1=v.v()
            mbs>>v;b2=v.v()
            iCount=b0|(b1<<8)|(b2<<16)
            n+=iCount<<8
            n>>=self.KShiftCardinality32
        else:
            raise IOError("Bad input")
        self.val=n

class HBuf:#Fixed DES also
    
    def __init__(self,mbs):
        nlen=TCardinality(mbs).val
        nsymb=nlen/2

        self.buf=mbs.getStr(nsymb)
