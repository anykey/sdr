## SDR
SDR (Symbian Data Recovery) is a set of tools for retrieving various data from Symbian (S60 Platform) phone backup files.

Two utilities are currently implemented:
- arcdump.py - extracts files from *.arc archieves.
- dbdump.py - exports EDBMS to the xml file.

## Requirements
python 2.7

## Basic usage
- Extract all system files: arcdump.py -f Backup.arc -d .
- Extract one file: arcdump.py -f Backup.arc -d . --filter "[.]*Notepad[.]*"
- Save DB tables as an xml dump:  dbdump.py -f DBS_101F8878_Notepad.dat -d Notepad.xml

## TODO
- Calendar
- Contacts
- Messaging
